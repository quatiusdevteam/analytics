Reference

https://developers.google.com/analytics/devguides/collection/protocol/v1/parameters

https://www.analyticsmania.com/post/datalayer-push

https://enhancedecommerce.appspot.com/checkout

        https://analytics.google.com/analytics/web/#/realtime/rt-overview/a126691113w185492581p182659516/

if (Request::session()->has('myga'))
        $my_ga = Request::session()->get('myga');
    else {
        $my_ga = uniqid();
        Request::session()->put('myga', $my_ga);
    }
    // https://php-ga-measurement-protocol.readthedocs.io/en/latest/
    // /https://github.com/theiconic/php-ga-measurement-protocol
    GAMP::addProduct( [
    'sku' => 'AAAA-5555',
    'name' => 'Test Product',
    'brand' => 'Test Brand',
    'category' => 'Test Category 1/Test Category 2',
    'variant' => 'blue',
    'price' => 85.00,
    'quantity' => 2,
    'coupon_code' => 'TEST',
    'position' => 4
]);

https://developers.google.com/analytics/devguides/collection/analyticsjs/enhanced-ecommerce
ga('ec:addProduct', {                 // Provide product details in an productFieldObject.
  'sku': 'P67890',                     // Product ID (string).
  'name': 'YouTube Organic T-Shirt',  // Product name (string).
  'category': 'Apparel/T-Shirts',     // Product category (string).
  'brand': 'YouTube',                 // Product brand (string).
  'variant': 'gray',                  // Product variant (string).
  'position': 2                       // Product position (number).
});

ga('ec:setAction', 'detail');
    

Snipcart.subscribe('item.added', function(item) {
        itemAdded(item);
        eventCategory: 'Cart Update',

        eventAction: 'New Item Added To Cart',
        eventLabel: item.name,
        eventValue: item.price,

        
        eventAction: 'Item Removed From Cart',
        eventLabel: item.name,
        eventValue: item.price,

        eventCategory: 'Cart Action',
        eventAction: 'Cart Opened',

        eventCategory: 'Cart Action',
        eventAction: 'Cart Closed',

        eventCategory: 'Page Change',
        eventAction: page,
        
        eventCategory: 'Order Update',
        eventAction: 'New Order Completed',

        purchase: {
                actionField: {
                    id: order.token,
                    affiliation: 'Website',
                    revenue: order.total,
                    tax: order.taxesTotal,
                    shipping: order.shippingInformation.fees,
                    invoiceNumber: order.invoiceNumber
                },
                products: createProductsFromItems(order.items),
                userId: order.user.id
            }
    });

    Snipcart.subscribe('item.removed', function(item) {
        itemRemoved(item);
    });

    Snipcart.subscribe('order.completed', function(order) {
        orderCompleted(order);
    });

    Snipcart.subscribe('cart.opened', function() {
        cartOpened();
    });

    Snipcart.subscribe('cart.closed', function() {
        cartClosed();
    });

    Snipcart.subscribe('page.change', function(page) {
        pageChanged(page);
    });

Action	Description
click	A click on a product or product link for one or more products.
detail	A view of product details.
add	Adding one or more products to a shopping cart.
remove	Remove one or more products from a shopping cart.
checkout	Initiating the checkout process for one or more products.
checkout_option	Sending the option value for a given checkout step.
purchase	The sale of one or more products.
refund	The refund of one or more products.
promo_click	A click on an internal promotion.