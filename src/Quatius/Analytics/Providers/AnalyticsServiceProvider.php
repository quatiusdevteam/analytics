<?php

namespace Quatius\Analytics\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Foundation\AliasLoader;
use Event;

use Quatius\Analytics\Events\AnalyticListeners;
use Request;
use Quatius\Analytics\Analytics;

class AnalyticsServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
		//$this->loadMigrationsFrom(__DIR__.'/../Databases/migrations');
        
        Event::subscribe(AnalyticListeners::class);
    }
    
    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('Analytics', function()
        {
            $isDisable = config('quatius.analytics.tracking_id', '')=='' || config('quatius.analytics.is_disabled', false);
            $analytics = new Analytics(config('quatius.analytics.is_ssl', false), $isDisable);
            return $analytics;
        });
        
        AliasLoader::getInstance()->alias('Analytics', 'Quatius\Analytics\Facades\AnalyticsFacade');
    }
    
    public function map()
    {
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['Analytics', Analytics::class];
    }
}
