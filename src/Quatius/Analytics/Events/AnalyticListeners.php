<?php

namespace Quatius\Analytics\Events;
use Request;
use Event;
use App;
use Analytics;

class AnalyticListeners {
    public function subscribe($events)
    {
        $events->listen('theme.events.before', function($theme){
            $theme->append('analytics-header', view("Analytics::page-header"));
        });

        $events->listen('analytics.send', function($type = "pageview", $reset=true){
            if (!Analytics::isActive()) return;
            
            $analytics = app('Analytics');
            if (!$analytics->getUserId() && user()->id){
                $analytics->setUserId(user()->id);
            }

            $type = strtolower($type);
            $response = [];
            if ($type == "event")
                $response = $analytics->sendEvent();
            elseif ($type == "screenview")
                $response = $analytics->sendScreenview();
            elseif ($type == "transaction")
                $response = $analytics->sendTransaction();
            elseif ($type == "item")
                $response = $analytics->sendItem();
            elseif ($type == "social")
                $response = $analytics->sendSocial();
            elseif ($type == "exception")
                $response = $analytics->sendException();
            elseif ($type == "timing")
                $response = $analytics->sendTiming();
            else
                $response = $analytics->sendPageview();
            
            if (config('quatius.analytics.debug', false))
                app('log')->debug('analytics.'.$type,[$response, $response->getDebugResponse()]);

            //if ($reset) App::forgetInstance('Analytics');
        });

        $events->listen('analytics.order', function($datas = [], $category="" , $action=""){
            
            if (!Analytics::isActive()) return;

            try{
                $dataSample = [
                    'reference'=>'xzyxxx123',
                    'currency_code'=> 'AUD',
                    'user_id'=> 0,
                    'revenue'=>100.00,
                    'tax'=>10.00,
                    'shipping'=>5.00,
                    'coupon_code' => 'Order TEST 2',
                    //'option'=>'paypal', // payment_methods
                    'items'=>[
                        [
                        'sku' => 'AAAA-6666',
                        'name' => 'Test Product 2',
                        'brand' => 'Test Brand 2',
                        'category' => 'Test Category 3/Test Category 4',
                        'variant' => 'yellow',
                        'price' => 50.00,
                        'quantity' => 1,
                        'coupon_code' => 'TEST 2',
                        'position'=> 1
                        ],
                        [
                            'sku' => 'AAAA-5555',
                            'name' => 'Test Product',
                            'brand' => 'Test Brand',
                            'category' => 'Test Category 1/Test Category 2',
                            'variant' => 'blue',
                            'price' => 85.00,
                            'quantity' => 2,
                            'coupon_code' => 'TEST',
                            'position'=> 2
                        ]
                    ]
                ];
                //$datas = $dataSample;
                
                if (is_array($datas)){
                    
                    $analytics = app('Analytics')->setAffiliation(array_get($datas,'affiliation',config('app.name', '')));
                    
                    if (array_has($datas,'user_id'))    
                        $analytics->setClientId(array_get($datas,'user_id',0));

                    if (array_has($datas,'transaction_id'))
                        $analytics->setTransactionId(array_get($datas,'transaction_id',''));
                    
                    if (array_has($datas,'currency_code'))
                        $analytics->setCurrencyCode(array_get($datas,'currency_code',''));
                    
                    if (array_has($datas,'checkout_step_option'))
                        $analytics->setCheckoutStepOption(array_get($datas,'checkout_step_option',''));
                    
                    if (array_has($datas,'revenue')){
                        $analytics->setRevenue(round(array_get($datas,'revenue',0),2))
                            ->setTax(round(array_get($datas,'tax', array_get($datas,'revenue',0)/11),2));
                    }
                
                    if (array_has($datas,'shipping'))
                        $analytics->setShipping(round(array_get($datas,'shipping',0),2));

                    if (array_has($datas,'coupon'))
                        $analytics->setCouponCode(array_get($datas,'coupon',''));

                    if(config('quatius.analytics.enhanced_ecommerce', true)){
                        $index = 0;
                        
                        foreach(array_get($datas,'items', []) as $product)
                        {
                            $index++;
                            $product['position'] = isset($product['position'])? $product['position']:$index;
                            if (strtolower($action) == "impressions")
                                $analytics->addProductImpression($product, $product['position']);
                            else
                                $analytics->addProduct($product);
                        }

                        if ($action)
                        {
                            $analytics->setEventAction($action);

                            //detail, click, add, remove, checkout, checkout_option, purchase, refund
                            if (strtolower($action) == "purchase") 
                                $analytics->setProductActionToPurchase();
                            elseif(strtolower($action) == "checkout")
                                $analytics->setProductActionToCheckout();
                            elseif(strtolower($action) == "checkout_option")
                                $analytics->setProductActionToCheckoutOption();
                            elseif(strtolower($action) == "refund")
                                $analytics->setProductActionToRefund();
                            elseif(strtolower($action) == "add")
                                $analytics->setProductActionToAdd();
                            elseif(strtolower($action) == "remove")
                                $analytics->setProductActionToRemove();
                            elseif(strtolower($action) == "click")
                                $analytics->setProductActionToClick();
                            elseif(strtolower($action) == "detail")
                                $analytics->setProductActionToDetail();
                        }
                        
                        if($category){
                            $analytics->setEventCategory($category);
                            Event::fire('analytics.send',['event']);
                        }
                    }
                    else{
                        
                        if ($category && $action)
                            Event::fire('analytics.send',['transaction']);
                        
                        foreach(array_get($datas,'items', []) as $product)
                        {
                            if (array_has($datas,'reference'))
                                $analytics->setTransactionId(array_get($datas,'reference',''));
                        
                            $analytics->setItemCode(array_get($product,'sku', ''));
                            $analytics->setItemName(array_get($product,'name', ''));
                            $analytics->setItemPrice(array_get($product,'price', 0));
                            $analytics->setItemQuantity(array_get($product,'quantity', 1));
                            $analytics->setItemCategory(array_get($product,'category', ''));
                            if ($category && $action)
                                Event::fire('analytics.send',['item']);
                        }
                        //if ($category && $action)
                            //App::forgetInstance('Analytics');
                    
                    }
                }
            
            }
            catch (\Exception $e){

            }
            
        });
        
        $events->listen('analytics.pageview.product', function($action="detail",$data = [])
		{
            if (!Analytics::isActive()) return;

            $analytics = app('Analytics');
            $analytics->setEventAction($action);
            if($data){
                if(config('quatius.analytics.enhanced_ecommerce', false)){
                    $analytics->addProduct($data);
                }
                else{
                    $analytics->setItemCode(array_get($data,'sku', ''));
                    $analytics->setItemName(array_get($data,'name', ''));
                    $analytics->setItemPrice(array_get($data,'price', 0));
                    $analytics->setItemCategory(array_get($data,'category', ''));
                }
            }
        });

        $events->listen('analytics.pageview', function($title="", $url="")
		{
            if (!Analytics::isActive()) return;

            $url = $url?:Request::path();
            $url = $url=="/"?"":$url;
            
            $analytics = app('Analytics')->setDocumentPath(config('quatius.analytics.url-prefix', '').'/'.$url);
            if ($title)
                $analytics->setDocumentTitle($title);

            Event::fire('analytics.send',['pageview']);
        });
    }
}