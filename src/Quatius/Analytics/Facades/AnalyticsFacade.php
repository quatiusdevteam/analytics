<?php 

namespace Quatius\Analytics\Facades;

use Illuminate\Support\Facades\Facade;

class AnalyticsFacade extends Facade
{
    protected static function getFacadeAccessor() {
        return 'Analytics';     
    }
}
