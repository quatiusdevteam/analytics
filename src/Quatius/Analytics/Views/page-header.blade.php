@php
    
    Event::fire('analytics.pageview',[Meta::get('title', config('app.name', 'Web Development'))]);
@endphp

<script type="text/javascript">
    var dataLayer = window.dataLayer = window.dataLayer || [];

    dataLayer.push({!!json_encode(Analytics::renderPayloads())!!});
    
</script>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','{!!config("quatius.analytics.tag_id","")!!}');
</script>
<!-- End Google Tag Manager -->

@script
<script type="text/javascript">
var analImpressions = {!! json_encode(Analytics::loadImpressionList('id'), JSON_HEX_TAG) !!};

var analTriggers =  {!! json_encode(Analytics::triggerPayloads(), JSON_HEX_TAG) !!}

function isGTMLoaded(){
    if (!!window.google_tag_manager) {
        // Google Tag Manager has already been loaded
        return true;
    } else {
        window.addEventListener('gtm_loaded', function() {
            // Google Tag Manager has been loaded
        });
    }
    return false;
}
function analyticActions(itm, action, callback, prepareCallback){
    if (!isGTMLoaded() && callback != null) callback(itm);

    if (analTriggers[action] == undefined) return true;
    
    var pushData = JSON.parse(JSON.stringify(analTriggers[action])); // clone

    if (prepareCallback != null){
        pushData = prepareCallback(pushData);
    }

    if(callback != null){
        pushData.eventCallback = function() {
            callback(itm);
        }
    }

    dataLayer.push(pushData);

    return false;
}

function clickImpression(e){
   
   var ref = $(e.target).parents('[data-sku]').data('sku');
   var link = $(e.target).parents('[href]').attr('href');

   if (!link)
       link = $(e.target).attr('href');
       
   if (analImpressions[ref] == undefined || !isGTMLoaded()) return true;

   var list = analImpressions[ref].list;

   dataLayer.push({
   'event': 'productClick',
   'ecommerce': {
     'click': {
       'actionField': {'list': list},      // Optional list property.
       'products': [analImpressions[ref]]
      }
    },
    'eventCallback': function() {
      document.location = link
    }
 });

 return false;
}

$(document).ready(()=>{
    $('{{config("quatius.analytics.dom-archor.product-click",".product-link")}}').on('click',(e)=>{
        return clickImpression(e);
    });

    $(document).on('{{config("quatius.analytics.js-event.cart-update","order-updated")}}',(evt, resp,itm)=>{
        $.each(resp, (key,data)=>{
            if (key == 'analytic_push'){
                dataLayer.push(data);
            }else if(key == 'analytic_triggers'){
                $.each(data, (key,data)=>{
                    analTriggers[key] = data;
                });
            }
        })
    });
});


</script>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id={!!config('quatius.analytics.tag_id','')!!}"
    height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

@endscript