<?php
namespace Quatius\Analytics;
use TheIconic\Tracking\GoogleAnalytics\Analytics as IconicAnalytics;
use Request;
use TheIconic\Tracking\GoogleAnalytics\Network\PrepareUrl;

class Analytics extends IconicAnalytics
{
    private $payloads = [];
    private $outputPayloads = [];
    private $products = [];
    private $productImpressions = [];
    
    public function __construct($isSsl = false, $isDisabled = false, array $options = [])
    {
        parent::__construct($isSsl, $isDisabled, $options);
        $this->initialise();
        $this->availableParameters['PayloadParameters'] = 'Quatius\Analytics\Analytics';
    }

    public function initialise()
    {
        $this->setProtocolVersion(config('quatius.analytics.protocol_version', '1'))
        ->setTrackingId(config('quatius.analytics.tracking_id', ''));
    
        if (config('quatius.analytics.anonymize_ip', false)) {
            $this->setAnonymizeIp('1');
        }

        if (config('quatius.analytics.async_requests', false)) {
            $this->setAsyncRequest(true);
        }
        
        try{
            if (Request::session()->has('quatius-analytics-clientId'))
                $clientId = Request::session()->get('quatius-analytics-clientId');
            else {
                $clientId = uniqid();
                Request::session()->put('quatius-analytics-clientId', $clientId);
            }
        }catch(\Exception $e){
            $clientId = uniqid();
        }

        $this->setClientId($clientId)
            ->setDebug(config('quatius.analytics.debug', false));

        if (!auth()->guest())
            $this->setUserId(auth()->user()->id);

        if (config('quatius.analytics.override_ip', '')){
            $this->setIpOverride(config('quatius.analytics.override_ip', ''));
        }
        else {
            $this->setIpOverride($this->getIp());
        }
        
        if(isset($_SERVER["HTTP_REFERER"])){
            $this->setDocumentReferrer($_SERVER["HTTP_REFERER"]);
        }
    }

    public function getIp(){
        $ipFound = null;
        foreach (array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR') as $key){
            if ($ipFound) break;

            if (array_key_exists($key, $_SERVER) === true){
                foreach (explode(',', $_SERVER[$key]) as $ip){
                    $ip = trim($ip); // just to be safe
                    if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) !== false){
                        $ipFound = $ip;
                        break;
                    }
                }
            }
        }

        if ($ipFound) return $ipFound;

        return request()->ip();
    }

    public function getPayloadParameters()
    {
        $prepareUrl = new PrepareUrl;

        $prepareUrl->build(
            $this->getEndpoint(),
            $this->singleParameters,
            $this->compoundParametersCollections
        );

        return $prepareUrl->getPayloadParameters();
    }

        /**
     * Sends a hit to GA. The hit will contain in the payload all the parameters added before.
     *
     * @param $methodName
     * @return AnalyticsResponseInterface
     * @throws Exception\InvalidPayloadDataException
     */
    protected function sendHit($methodName)
    {
        $this->savePostData($methodName);
        
        if(!$this->isTagActive() && $this->getEventCategory() != 'trigger')
            return parent::sendHit($methodName);
    }

    public function savePostData($methodName){
        if (config('quatius.analytics.show-log', false))
            app('log')->debug('Analytics Post',[$this->getEndpoint(), $this->getPayloadParameters()]);
        
        $payloads = $this->getPayloadParameters();
        $payloads["products"] = $this->products;
        $methodName = $this->getEventCategory()=='trigger'?'trigger':$methodName;
        
        $this->payloads[$methodName.":".($this->getEventAction()?:"")] = $payloads;

        $this->products = [];
        $this->singleParameters = [];
        $this->compoundParametersCollections = [];
        $this->initialise();
    }

    public function addProduct($product){
        $this->products[] = json_decode(json_encode($product), true); // clone
        unset($product['list']);
        
        parent::addProduct($product);
    }

    public function addProductImpression($product, $index)
    {
        $this->productImpressions[] = json_decode(json_encode($product), true); // clone
        $list  = $product['list'];
        unset($product['list']);

        $this->setProductImpressionListName($list, $index<200?$index:1);

        parent::addProductImpression($product, $index<200?$index:1);
    }

    public function isActive(){
        return config('quatius.analytics.tracking_id', '') || config('quatius.analytics.tag_id', ''); 
    }

    public function isTagActive(){
        return !!config('quatius.analytics.tag_id', ''); 
    }

    public function loadImpressionList($keyBy=''){
        return $this->loadProducts($this->productImpressions, $keyBy);
    }

    private function loadProducts($products=[], $keyBy=''){
        $prodOuts = [];
        foreach($products as $prod ){
            $prods = [
                'name'=> array_get($prod,'name',''),
                'id'=> array_get($prod,'sku','')
            ];

            if (array_has($prod,'price')) $prods['price'] = round(array_get($prod,'price',''),2);
            if (array_has($prod,'category')) $prods['category'] = array_get($prod,'category','');
            if (array_has($prod,'variant')) $prods['variant'] = array_get($prod,'variant','');
            if (array_has($prod,'quantity'))  $prods['quantity'] = intval(array_get($prod,'quantity','0'));
            if (array_has($prod,'coupon_code')) $prods['coupon'] = array_get($prod,'coupon_code','');
            if (array_has($prod,'custom_dimension_1')) $prods['dimension1'] = array_get($prod,'custom_dimension_1','');
            if (array_has($prod,'list')) $prods['list'] = array_get($prod,'list','');
            if (array_has($prod,'position')) $prods['position'] = array_get($prod,'position','');

            if (array_has($prod,'brand')) 
                $prods['brand'] = array_get($prod,'brand','');
            elseif(config('quatius.analytics.product-default.brand',''))
                $prods['brand'] = config('quatius.analytics.product-default.brand','');

            $prodOuts[] =$prods;
        }
        if ($keyBy)
            return collect($prodOuts)->keyBy($keyBy)->toArray();
        else
            return $prodOuts;
    }

    private function preparePayload($datas = []){
        $this->outputPayloads = array_merge($this->outputPayloads, $datas);
    }

    private function outputPayload($datas = []){
        return array_merge(config('quatius.analytics.data_layer_extras',[]), $this->outputPayloads);
    }
    
    public function triggerPayloads(){
        $triggers = [];
        foreach($this->payloads as $type=>$data)
        {
            if ($type == "trigger:checkout"){
                $triggers["checkout"] = [
                    'event'=> 'checkout',
                    'ecommerce' => [
                        'checkout' => [
                            'actionField'=> ['step'=> 1],
                            'products'=> $this->loadProducts(array_get($data,'products',[]))
                        ]
                    ]
                ];
            }elseif ($type == "trigger:checkout_step_option"){
                $triggers["checkout_step_option"] = [
                    'event'=> 'checkoutOption',
                    'ecommerce' => [
                        'checkout_option' => [
                            'actionField'=> ['step'=> 1]
                        ]
                    ]
                ];
            }
        }
        return $triggers;
    }

    public function renderPayloads(){
        foreach($this->payloads as $type=>$data)
        {
            if ($type == "sendPageview:"){
                $this->preparePayload([
                    'pagePath' => array_get($data,'dp',''),
                    'pageTitle' => array_get($data,'dt','')
                ]);
            }
            elseif ($type == "sendEvent:detail"){
                $this->preparePayload([
                    'ecommerce' => [
                        'detail' => [
                            'products'=> $this->loadProducts(array_get($data,'products',[]))
                        ]
                    ]
                ]);
            }
            elseif ($type == "sendEvent:add"){
                $this->preparePayload([
                    'event'=> 'addToCart',
                    'ecommerce' => [
                        'currencyCode'=> array_get($data,'cu','AUD'),
                        'add' => [
                            'products'=> $this->loadProducts(array_get($data,'products',[]))
                        ]
                    ]
                ]);
            }
            elseif ($type == "sendEvent:remove"){
                $this->preparePayload([
                    'event'=> 'removeFromCart',
                    'ecommerce' => [
                        'remove' => [
                            'products'=> $this->loadProducts(array_get($data,'products',[]))
                        ]
                    ]
                ]);
            }
            // elseif ($type == "sendEvent:review"){
            //     $this->preparePayload([
            //         'event'=> 'checkout',
            //         'ecommerce' => [
            //             'checkout' => [
            //                 'actionField'=> ['step'=> 1],
            //             ]
            //         ]
            //     ]);
            // }
            elseif ($type == "sendEvent:checkout"){
                $this->preparePayload([
                    'event'=> 'checkout',
                    'ecommerce' => [
                        'checkout' => [
                            'actionField'=> ['step'=> 1],
                            'products'=> $this->loadProducts(array_get($data,'products',[]))
                        ]
                    ]
                ]);
            }elseif ($type == "sendEvent:purchase"){
                $this->preparePayload([
                    //'event'=> 'checkoutOption',
                    'ecommerce' => [
                        // 'checkout_option' => [
                        //     'actionField'=> ['step'=> 4]
                        // ],
                        'purchase' => [
                            'currencyCode'=> array_get($data,'cu','AUD'),
                            'actionField'=> [
                                'step'=> 4,
                                'id' => array_get($data,'ti',''),
                                'affiliation' => array_get($data,'ta',''),
                                'revenue' => array_get($data,'tr',0),
                                'tax' => array_get($data,'tt',0),
                                'shipping' => array_get($data,'ts',0),
                                'coupon' => array_get($data,'tcc',''),
                            ],
                            'products'=> $this->loadProducts(array_get($data,'products',[]))
                        ]
                    ]
                ]);
            }elseif ($type == "sendEvent:impressions"){
                $this->preparePayload([
                    'ecommerce' => [
                        'currencyCode'=> array_get($data,'cu','AUD'),
                        'impressions'=> $this->loadProducts($this->productImpressions)
                    ]
                ]);
            }
        }

        return $this->outputPayload();
    }
}